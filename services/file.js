const {makeScanQueryUsingFileHash} = require("../utils/query");

const {makeHashOfFile, patchFileName} = require("../utils/hash");
const {fileStorageModule, dataBaseModule} = require('./../modules');

const uploadFileFromRequest = async ({originalname: originalName, mimetype: mimeType, buffer}) => {
    if (!Buffer.isBuffer(buffer)) buffer = new Buffer.from(buffer);
    const name = patchFileName(originalName);
    await fileStorageModule.uploadFile(buffer, name);
    return makeHashOfFile(buffer, originalName)
};
const getValueFromDynamoResponse = dynamoObj => Object.values(dynamoObj)[0];

const getThumbnails = async fileHash => {
    const query = makeScanQueryUsingFileHash(fileHash);

    const {Items} = await dataBaseModule.scan(query);
    return Items.filter(i => i.url && i.pageNumber)
        .map(i => {
            return {
                url: getValueFromDynamoResponse(i.url),
                page: Number( getValueFromDynamoResponse(i.pageNumber))
            }
        })
};

module.exports = {
    getThumbnails,
    uploadFileFromRequest
};
