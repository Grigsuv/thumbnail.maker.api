const express = require('express');
const app = express();

const routes = require('./routes');
const {healthyResponse, setGlobalMiddleware, errorHandler, notFound} = require("./middlewares");

setGlobalMiddleware(app);

app.get('/', healthyResponse);
app.use('/api', routes);
app.use(notFound);
app.use(errorHandler);

module.exports = app;
