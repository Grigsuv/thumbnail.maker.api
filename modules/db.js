const {DynamoDB} = require('aws-sdk');
const {fileModel, thumbnailsModel} = require('./../models');
const apiVersion = '2012-08-10';

class DataBase {

    static tables = [fileModel, thumbnailsModel];

    #DBInstance;

    #init = async () => {
        const {TableNames} = await this.#DBInstance.listTables().promise();
        DataBase.tables.forEach(({name, schema}) => {
            if (!TableNames.includes(name)) {
                this.#createTable(schema)
            }
        })
    };

    #createTable = async tableSchema => {
        await this.#DBInstance.createTable(tableSchema).promise();
        throw new Error('Table was created but restarted needed')
    };

    constructor({accessKeyId, secretAccessKey, dataBaseRegion}) {
        this.#DBInstance = new DynamoDB({accessKeyId, secretAccessKey, region: dataBaseRegion, apiVersion});
        this.#init()
            .catch(e => {
                console.log(e);
                process.exit(-1)
            })
    }

    addItem(insertParams) {
        return this.#DBInstance.putItem(insertParams).promise();
    }

    batchWrite(items) {
        return this.#DBInstance.batchWriteItem({RequestItems: items}).promise()
    }

    updateItem(updateExpression) {
        return this.#DBInstance.updateItem(updateExpression).promise()
    }
    scan(scanExpression){
        return this.#DBInstance.scan(scanExpression).promise()
    }
}

module.exports = DataBase;
