const generateError = (statusCode, text) => {
    const e = new Error();
    e.status = statusCode || 500;

    if(typeof text !== "undefined") e.response = text;
    return e
};


module.exports = {
    generateError
};
