const dbQueryTemplate = {
    ExpressionAttributeValues: {
        ":a": {
            S: '$$fileHash$$'
        }
    },
    FilterExpression: "fileHash = :a",
    TableName: "thumbnail-maker-thumbnails"
};

const makeScanQueryUsingFileHash = fileHash => {
    return JSON.parse(JSON.stringify(dbQueryTemplate).replace('$$fileHash$$', fileHash))
};

module.exports = {
    makeScanQueryUsingFileHash
};
