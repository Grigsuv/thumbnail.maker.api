const normalizePort = val => {
    let port = parseInt(val, 10);

    if (isNaN(port)) return false

    return port
}
const configParser = {
    PORT: {
        parser: port => normalizePort(port),
        validator : port => port >= 80
    },
}
module.exports = {
    configParser
}