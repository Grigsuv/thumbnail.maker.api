const crypto = require('crypto');

const makeHashOfFile = (fileBuffer, name) => {
    if (!Buffer.isBuffer(fileBuffer)) fileBuffer = new Buffer.from(fileBuffer);
    let sum = crypto.createHash('sha256');
    sum.update(fileBuffer);
    if (name) sum.update(Buffer.from(name));
    return sum.digest('hex');
};
const patchFileName = fileName => {
    const splitedName = fileName.split('.');
    const ext = splitedName.pop()
    return `${splitedName.join('.')}:${new Date().getTime()}.${ext}`
};
module.exports = {
    makeHashOfFile,
    patchFileName
};
