# Thumbnail.Maker.API

### Description
The project is made for creating thumnails from the file. It's uses `DynamoDB` for DB store management

### Currently supported file types

```
pdf
jpg
png
```

### Create .env file with following keys

```
PORT
AWS_REGION
AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY
```
