module.exports = {
    fileModel: require('./files'),
    thumbnailsModel: require('./thumbnails')
};
