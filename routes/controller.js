const {fileService} = require("../services");
const {generateError} = require('./../utils/response');

const uploadFile = async (req, res, next) => {
    try {
       const hash = await fileService.uploadFileFromRequest(req.file);
        res.json({hash})
    }catch (e) {
        next(e)
    }
};

const getThumbnails = async (req, res, next) => {
    try {
        const {fileHash} = req.params;
        const thumbnails = await fileService.getThumbnails(fileHash);
        if(!thumbnails || thumbnails.length === 0 ) return next(generateError(404, `${fileHash} dosent exists or not processed yet`));
        res.json(thumbnails)
    } catch (e) {
        next(e)
    }
};

module.exports = {
    getThumbnails,
    uploadFile
};
