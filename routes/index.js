const express = require('express');
const router = express.Router();

const {getSingleFileBufferFromMultipart, contentTypeValidator} = require("../middlewares");
const {uploadFile, getThumbnails} = require("./controller");

router.post('/files', getSingleFileBufferFromMultipart('file'), contentTypeValidator, uploadFile);
router.get('/thumbnails/:fileHash', getThumbnails);

module.exports = router;
