const helmet = require('helmet');
const multer = require('multer');
const upload = multer();
const {generateError} = require('./../utils/response');

const errorCodesAndMessages = {
    '400': {
        message: "UNPROCESSABLE ENTITY"
    },
    '404': {
        message: "NOT FOUND"
    },
    '500': {
        message: "INTERNAL ERROR"
    }
};

const setGlobalMiddleware = app => {
    app.use(helmet());
    app.disable('x-powered-by');
};

const healthyResponse = (req, res) => {
    res.send('Healthy')
};
const notFound = (req, res, next) => {
    return next(generateError(404));
};
const errorHandler = (err, req, res, next) => {
    let status = err.status || 500;

    if (status === 404) console.log(`${req.method} ${req.originalUrl} 404`);

    else console.log(err);

    res.status(status);
    res.json({
        error: err.response || errorCodesAndMessages[status],
    });
};

const getSingleFileBufferFromMultipart = fileKey => upload.single(fileKey);

const validTypes = ['image/jpeg', 'image/png', "application/pdf"];

const contentTypeValidator = (req, res, next) => {
    const {file} = req;
    if (!file) return next(generateError(400, 'File is missing'));

    const {mimetype: fileType} = file;

    if (!validTypes.includes(fileType)) return next(generateError(400, 'File type not supported'));

    next()
};
module.exports = {
    setGlobalMiddleware,
    healthyResponse,
    notFound,
    errorHandler,
    getSingleFileBufferFromMultipart,
    contentTypeValidator
};
